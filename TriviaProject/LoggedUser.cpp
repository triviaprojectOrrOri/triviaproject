#include "LoggedUser.h"




LoggedUser::LoggedUser(std::string username): m_username(username)
{
}

LoggedUser::~LoggedUser()
{
}

std::string LoggedUser::getUsername() const
{
	return this->m_username;
}

bool LoggedUser::operator<(const LoggedUser& other) const
{
	return m_username < other.getUsername();
}

bool LoggedUser::operator>(const LoggedUser& other) const
{
	return m_username > other.getUsername();
}

bool LoggedUser::operator==(const LoggedUser & other) const
{
	return this->getUsername() == other.getUsername();
}
