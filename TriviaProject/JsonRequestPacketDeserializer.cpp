#include "JsonRequestPacketDeserializer.h"

void printPtr(const aVec & vec, const unsigned int len)
{
	printf("\n");
	for (unsigned int i = 0; i < len; i++)
	{
		printf("%u ", (unsigned char)vec[i].to_ulong());
	}
}
//performs str slicing and returns an aVec
aVec slice(const aVec& vec, const unsigned int start, const unsigned int len)
{
	aVec sliced(len);
	for (unsigned int i = 0; i < len; i++) {
		sliced[i] = (char)vec[i + start].to_ulong();
	}
	return sliced;
}

std::string JsonRequestPacketDeserializer::extractField(const std::string& buffer)
{
	unsigned char len = (unsigned char)	buffer[0];
	return buffer.substr(CODE_END, len);
}

std::string JsonRequestPacketDeserializer::extractData(const aVec& buffer)
{
	unsigned int len = 0;
	aVec sliced = slice(buffer, CODE_END, DATALEN_LEN);
	std::string toRet = "";
	memcpy(&len, sliced.data(), DATALEN_LEN);
	sliced = slice(buffer, CODE_END + DATALEN_LEN, len);
	for (unsigned int i = 0; i < len; i++) {
		toRet += (char)sliced[i].to_ulong();
	}
	return toRet;
}

unsigned int JsonRequestPacketDeserializer::extractUnsignedInt(const std::string & msg)
{
	return stoi(msg.substr(0, sizeof(int)));
}

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(const aVec& buffer)
{
	LoginRequest request;
	std::string data = JsonRequestPacketDeserializer::extractData(buffer);
	nlohmann::json jsonObj = nlohmann::json::parse(data);
	std::string tempStr = std::string(jsonObj.value("username", "unknown"));
	request.username = tempStr;
	tempStr = std::string(jsonObj.value("password", "unknown"));
	request.password =tempStr;
	return request;
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(const aVec& buffer)
{
	SignupRequest request;
	std::string data = JsonRequestPacketDeserializer::extractData(buffer);
	nlohmann::json jsonObj = nlohmann::json::parse(data);
	std::string tempStr = std::string(jsonObj.value("username", "unknown"));
	request.username =tempStr;
	tempStr = std::string(jsonObj.value("password", "unknown"));
	request.password = tempStr;
	tempStr = std::string(jsonObj.value("email", "unknown"));
	request.email = tempStr;
	return request;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(const aVec& buffer)
{
	GetPlayersInRoomRequest request;
	std::string data = JsonRequestPacketDeserializer::extractData(buffer);
	nlohmann::json jsonObj = nlohmann::json::parse(data);
	unsigned int tempStr = jsonObj.value("roomId", 0);
	request.roomId = tempStr;
	return request;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(const aVec & buffer)
{
	JoinRoomRequest request;
	std::string data = JsonRequestPacketDeserializer::extractData(buffer);
	nlohmann::json jsonObj = nlohmann::json::parse(data);
	unsigned int tempStr = jsonObj.value("roomId", 0);
	request.roomId = tempStr;
	return request;
}
CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomReuquest(const aVec& buffer)
{
	CreateRoomRequest request;
	std::string data = JsonRequestPacketDeserializer::extractData(buffer);
	nlohmann::json jsonObj = nlohmann::json::parse(data);
	std::string tempStr = std::string(jsonObj.value("roomName", "unknown"));
	request.roomName = tempStr;
	unsigned int tempNum = jsonObj.value("maxUsers", 0);
	request.maxUsers = tempNum;
	tempNum = jsonObj.value("questionCount", 0);
	request.questionCount = tempNum;
	tempNum = jsonObj.value("answerTimeout", 0);
	request.answerTimeout = tempNum;
	tempNum = jsonObj.value("questionCount", 0);
	request.questionCount = tempNum;
	return request;
}




