#include "JsonDeserilizerTester.h"

aVec JsonDeserilizerTester::createLogInBuffer()
{
	std::string str("21111{ \"username\": \"ori\", \"password\": \"1234\"}");
	aVec vec;
	vec.resize(str.size(), 0);
	for (unsigned int i = 0; i < str.size(); i++) {
		vec[i] = str[i];
	}
	unsigned int fieldSize = (unsigned int)strlen("{ \"username\": \"ori\", \"password\": \"1234\"}");
	memcpy(vec.data() + DATA_LEN_BYTE, &fieldSize, sizeof(char) * DATA_LEN_BYTES);
	return vec;
}

aVec JsonDeserilizerTester::createSignUpBuffer()
{
	std::string str("21111{ \"username\": \"ori\", \"password\": \"1234\", \"email\": \"orikoj10\" }");
	aVec vec;
	vec.resize(str.size(), 0);
	for (unsigned int i = 0; i < str.size(); i++) {
		vec[i] = str[i];
	}
	unsigned int fieldSize = (unsigned int)strlen("{ \"username\": \"ori\", \"password\": \"1234\", \"email\": \"orikoj10\" }");
	memcpy(vec.data() + DATA_LEN_BYTE, &fieldSize, sizeof(char) * DATA_LEN_BYTES);
	return vec;
}

bool JsonDeserilizerTester::checkDesirializer()
{
	LoginRequest loginRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(createLogInBuffer());
	if (loginRequest.username != "ori" || loginRequest.password != "1234")
	{
		return false;
	}
	SignupRequest signupRequest = JsonRequestPacketDeserializer::deserializeSignupRequest(createSignUpBuffer());
	if (signupRequest.username != "ori" || signupRequest.password != "1234" || signupRequest.email != "orikoj10")
	{
		return false;
	}
	return true;
}
