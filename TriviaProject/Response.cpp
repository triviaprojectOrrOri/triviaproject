#include "Response.h"


std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(std::map<std::string, std::string> typesVals, int responseType)
{
	std::string strDump = createJsonDict(typesVals);
	std::vector<Byte> toRet(strDump.length() + BASIC_LEN);
	char * msgLen = new char[sizeof(int)];
	int len = strDump.length();

	memcpy(msgLen, &len, sizeof(int));

	toRet[MSG_TYPE] = responseType;
	for (unsigned int i = SKIP_TYPE; i < BASIC_LEN; i++)
		toRet[i] = msgLen[i - SKIP_TYPE];

	delete msgLen;

	for (unsigned int i = 0; i < strDump.length(); i++)
		toRet[i + BASIC_LEN] = strDump[i];
	return toRet;
}

std::string JsonResponsePacketSerializer::createJsonDict(std::map<std::string, std::string> typesVals)
{
	std::string toParse = "{";
	for (auto it = typesVals.begin(); it != typesVals.end(); it++) {
		if (it != typesVals.begin()) {
			toParse += ",";
		}
		toParse += "\"" + it->first + "\":" + it->second;
	}
	toParse += "}";
	return toParse;
}

std::string JsonResponsePacketSerializer::convertToStr(std::string toConvert)
{
	return "\"" + toConvert + "\"";
}

std::string JsonResponsePacketSerializer::convertToStr(Highscore toConvert)
{
	return "[\"username\":\"" + toConvert.username + "\", \"score\":" + std::to_string(toConvert.score) + "]";
}

std::string JsonResponsePacketSerializer::convertToStr(RoomData toConvert)
{
	return "{\"id\":" + std::to_string(toConvert.id) + ", \"isActive\":" + std::to_string(toConvert.isActive) + ",\"timePerQuestion\":" + std::to_string(toConvert.timePerQuestion) + ",\"maxPlayers\":" + std::to_string(toConvert.maxPlayers) + ",\"questionsCount\":" + std::to_string(toConvert.questionsCount) + ",\"admin\":\"" + toConvert.name + "\"}";
}

template<class T>
std::string JsonResponsePacketSerializer::makeVecToStr(std::vector<T> vec)
{
	std::string toReturn = "[";
	for (auto it = vec.begin(); it != vec.end(); it++) {
		if (it != vec.begin()) {
			toReturn += ",";
		}
		toReturn += convertToStr(*it);
	}
	toReturn += "]";
	return toReturn;

}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(ErrorResponse msgType)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "message", "\"" + msgType.message + "\"" });
	return serializeResponse(typesValues, ERR_CODE);
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(LoginResponse msgType)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(msgType.status)});
	return serializeResponse(typesValues, LOGIN_CODE);
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(SignupResponse msgType)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(msgType.status)});
	return serializeResponse(typesValues, SIGNUP_CODE);
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(LogoutResponse msgType)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(msgType.status) });
	return serializeResponse(typesValues, LOGOUT_CODE);
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse msgType)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(msgType.status) });
	return serializeResponse(typesValues, CREATE_ROOM_CODE);
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse msgType)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(msgType.status) });
	return serializeResponse(typesValues, JOIN_ROOM_CODE);
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse msgType)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "players", makeVecToStr(msgType.players) });
	return serializeResponse(typesValues, GET_PLAYERS_IN_ROOM_CODE);
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse msgType)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(msgType.status) });
	typesValues.insert({ "rooms", makeVecToStr(msgType.rooms) });
	return serializeResponse(typesValues, GET_ROOMS_CODE);
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(HighscoreResponse msgType)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(msgType.status) });
	typesValues.insert({ "highscores", makeVecToStr(msgType.highscores) });
	return serializeResponse(typesValues, HIGHSCORE_CODE);
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse msgType)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(msgType.status) });
	return serializeResponse(typesValues, CLOSE_ROOM_CODE);
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(StartGameResponse msgType)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(msgType.status) });
	return serializeResponse(typesValues, CLOSE_ROOM_CODE);
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse msgType)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(msgType.status) });
	return serializeResponse(typesValues, CLOSE_ROOM_CODE);
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse msgType)
{
	std::map<std::string, std::string> typesValues;
	typesValues.insert({ "status", std::to_string(msgType.status) });
	typesValues.insert({ "answerTimeout", std::to_string(msgType.answerTimeout) });
	typesValues.insert({ "hasGameBegun", std::to_string(msgType.hasGameBegun) });
	typesValues.insert({ "players", makeVecToStr(msgType.players) });
	typesValues.insert({ "questionCount", std::to_string(msgType.questionCount) });
	return serializeResponse(typesValues, CLOSE_ROOM_CODE);
}