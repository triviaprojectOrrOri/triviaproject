#pragma once
#include <string>
class LoggedUser
{
private:
	std::string m_username;
public:
	LoggedUser(std::string);
	~LoggedUser();
	std::string getUsername() const;
	bool operator<(const LoggedUser &other) const;
	bool operator>(const LoggedUser &other) const;
	bool operator==(const LoggedUser &other) const;
};

