#pragma once
#include "JsonRequestPacketDeserializer.h"
#define FIRST_FILED_LEN_BYTE 5
#define SECOND_FILED_LEN_BYTE 14
#define THIRD_FILED_LEN_BYTE 23
#define DATA_LEN_BYTE 1
#define DATA_LEN_BYTES 4
#define LOGIN_MESSAGE_LEN 45
#define SIGNUP_MESSAGE_LEN 67
class JsonDeserilizerTester
{
	//these two functions create bufferst that are passed to the deserializer
	aVec createLogInBuffer();
	aVec createSignUpBuffer();
public:
	bool checkDesirializer();//returns true if check successful
};

