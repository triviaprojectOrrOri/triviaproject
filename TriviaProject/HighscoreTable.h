#pragma once
#include "IDatabase.h"

class HighscoreTable
{
private:
	IDatabase* m_database;
public:
	HighscoreTable(IDatabase * db);
	~HighscoreTable();
	std::map<LoggedUser, int> getHighscores();
};

