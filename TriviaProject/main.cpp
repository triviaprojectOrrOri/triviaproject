#pragma once
#include <iostream>
#include "Server.h"
#include "SqliteDatabase.h"

int main()
{
	SqliteDatabase db;
	Server s(&db, NOT_TEST);
	s.run();
	return 0;
}