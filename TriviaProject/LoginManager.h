#pragma once
#include <vector>
#include <string>
#include <mutex>
#include "LoggedUser.h"
#include "IDatabase.h"
class LoginManager
{
private:
	static std::mutex changeLoggedInMtx;
	IDatabase * m_database;
	std::vector<LoggedUser> m_loggedUsers;
public:
	LoginManager(IDatabase * database);
	~LoginManager();
	void signup(std::string username, std::string password, std::string email);
	void login(std::string username, std::string password);
	void logout(std::string username);
	bool isUserLoggedIn(std::string username) const;
};

