#pragma once
#include <iostream>
#include "Server.h"
#include "SqliteDatabase.h"
#include "ResponseTest.h"


#define USERNAME_TO_USE "Orr"
#define PASSWORD_TO_USE "321123"
#define EMAIL_TO_USE "orr.sharon.orr@gmail.com"
#define TYPE_MSG LOGIN
#define LOCAL_HOST "127.0.0.1"
#define EXPECTED_RESULT true

class InfrastructureTester
{
private:
	std::vector<char> getMessageToSend();
	bool connectToServer();
public:
	InfrastructureTester();
	~InfrastructureTester();
	bool test();
};

