#include "RoomMemberRequestHandler.h"




RequestResult RoomMemberRequestHandler::leaveRoom(Request rqst)
{
	RequestResult result;
	try {
		CloseRoomResponse response;
		m_roomManager->removePlayer(m_user, m_roomID);
		result.newHandle = m_handlerFactory->createMenuRequestHandler(m_user.getUsername());
		response.status = true;
		result.response = JsonResponsePacketSerializer::serializeResponse(response);
	}
	catch (std::exception e) {
		error(e.what(), result);
	}
	return result;
}

RequestResult RoomMemberRequestHandler::getRoomState(Request rqst)
{
	RequestResult result;
	try {
		GetRoomStateResponse response;
		Room roomToGet = m_roomManager->getRoomById(m_roomID);
		RoomData metadata = roomToGet.getMetadata();
		std::vector<LoggedUser> loggedUsers = roomToGet.getAllUsers();
		result.newHandle = m_handlerFactory->createRoomAdminRequestHandler(m_user.getUsername(), m_roomID);

		response.status = true;
		response.answerTimeout = metadata.timePerQuestion;
		response.hasGameBegun = metadata.isActive;
		response.questionCount = metadata.questionsCount;

		response.players;
		for (auto it = loggedUsers.begin(); it != loggedUsers.end(); it++) {
			response.players.push_back(it->getUsername());
		}
		result.response = JsonResponsePacketSerializer::serializeResponse(response);
	}
	catch (std::exception e) {
		error(e.what(), result);
	}
	return result;
}

void RoomMemberRequestHandler::error(const char * errorMsg, RequestResult & toReturn)
{
	if (strcmp(errorMsg, "Error - room doesn't exist")) {
		toReturn.newHandle = m_handlerFactory->createMenuRequestHandler(m_user.getUsername());
	}
	else {
		toReturn.newHandle = m_handlerFactory->createRoomMemberRequestHandler(m_user.getUsername(), m_roomID);
	}
	
	ErrorResponse errRes = { errorMsg };
	toReturn.response = JsonResponsePacketSerializer::serializeResponse(errRes);
}

bool RoomMemberRequestHandler::isRequestRelevant(const Request & rqst) const
{
	if (rqst.id >= CLOSE_OR_LEAVE_ROOM && rqst.id <= GET_ROOM_STATE) {
		return true;
	}
	return false;
}

RequestResult RoomMemberRequestHandler::handleRequest(const Request & rqst)
{
	switch (rqst.id) {
	case CLOSE_OR_LEAVE_ROOM:
		return leaveRoom(rqst);
	case GET_ROOM_STATE:
		return getRoomState(rqst);
	default:
		//if the request isn't of any of the possible types
		RequestResult toReturn;
		toReturn.newHandle = nullptr;
		ErrorResponse errRes;
		errRes.message = "Error - request code not recognized";
		toReturn.response = JsonResponsePacketSerializer::serializeResponse(errRes);
		return toReturn;
	}
}

int RoomMemberRequestHandler::getType()
{
	return ROOM_MEMBER_REQUEST_HANDLER;
}

RoomMemberRequestHandler::RoomMemberRequestHandler(LoggedUser user, roomID id, RoomManager * roomManager, RequestHandlerFactory * handlerFactory) : m_handlerFactory(handlerFactory), m_roomManager(roomManager), m_roomID(id), m_user(user)
{
}

RoomMemberRequestHandler::~RoomMemberRequestHandler()
{
}
