#include "Question.h"




Question::Question(std::string question, std::vector<std::string> possibleAnswers, int correct) : m_question(question), m_possibleAnswers(possibleAnswers), m_correct(correct)
{}

Question::~Question()
{}

std::string Question::getQuestion() const
{
	return m_question;
}

std::vector<std::string> Question::getPossibleAnswers() const
{
	return m_possibleAnswers;
}

std::string Question::getCorrectAnswer() const
{
	return m_possibleAnswers[m_correct];
}
