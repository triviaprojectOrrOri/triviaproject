#pragma once
#include "RequestHandlerFactory.h"
#include "IRequestHandler.h"
#include "LoginManager.h"
#include "Response.h"
#include "MenuRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "sqlite3.h"

#define TYPE_BYTE 1
#define LEN_BYTE 1
#define ZERO_END 1


enum {
	LOGIN = 1,
	SIGNUP
};

class RequestHandlerFactory;

class LoginRequestHandler: public IRequestHandler
{
	LoginManager* m_loginManager;
	RequestHandlerFactory *m_handlerFactory;
	RequestResult login(const Request& rqst);//translates the request and calls LoginManager
	RequestResult signup(const Request& rqst);//translates the request and calls LoginManager
	void error(const char * errorMsg, RequestResult& toReturn);//in case of error
public:
	virtual bool isRequestRelevant(const Request& rqst) const override;
	virtual RequestResult handleRequest(const Request& rqst) override;//calls one of the two functions
	virtual int getType() override;
	LoginRequestHandler(LoginManager *loginManager, RequestHandlerFactory* handlerFactory);
	~LoginRequestHandler();
};
