#pragma once
#include <ctime>
#include <memory>
#include <vector>
#include <bitset>

#define LOGIN_REQUEST_HANDLER 0
#define MENU_REQUEST_HANDLER 1
#define ROOM_MEMBER_REQUEST_HANDLER 2
#define ROOM_ADMIN_REQUEST_HANDLER 3

typedef std::bitset<8> Byte;


typedef struct Request {
	time_t receivalTime;
	std::vector<Byte> buffer;
	int id;
} Request;

typedef struct RequestResult {
	std::vector<Byte> response;
	class IRequestHandler * newHandle;
} RequestResult;

class IRequestHandler
{
public:
	virtual bool isRequestRelevant(const Request& rqst) const = 0;
	virtual RequestResult handleRequest(const Request& rqst) = 0;
	virtual int getType() = 0;
	IRequestHandler();
	virtual ~IRequestHandler();
};

