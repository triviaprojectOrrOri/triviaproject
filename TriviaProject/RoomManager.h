#pragma once
#include <map>
#include "Room.h"
typedef unsigned int roomID;

#define NOT_ACTIVE 0


class RoomManager
{
	std::map<roomID, Room> m_rooms;
	unsigned int m_nextRoomNum;
public:
	RoomManager();
	~RoomManager();
	int createRoom(LoggedUser admin, RoomData metadata);
	void deleteRoom(const roomID id);
	void joinRoom(LoggedUser user, roomID id);
	void removePlayer(LoggedUser user, roomID id);
	//getters
	unsigned int getRoomState(roomID ID);
	std::vector<RoomData> getRooms() const;
	Room getRoomById(roomID roomId) const;
	//setter
	void setRoomState(roomID ID, bool newState);
};

