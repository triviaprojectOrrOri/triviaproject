#include <iostream>
#include "LoginRequestHandlerTester.h"
#include "LoginRequestHandler.h"
#include "SqliteDatabase.h"
#include "RequestHandlerFactory.h"
#define DATA_LEN_BYTE 1
#define DATA_LEN_BYTES 4
void change(Request * rqst, std::string toEnter) {
	rqst->buffer.push_back((unsigned char)toEnter.length());
	for (unsigned int i = 0; i < toEnter.length(); i++) {
		rqst->buffer.push_back(toEnter[i]);
	}
}

void checkMsg(std::vector<Byte> msgVec) {
	int size;
	memcpy(&size, msgVec.data() + SKIP_TYPE, sizeof(int));

	std::cout << "Message type: " << msgVec[MSG_TYPE].to_ulong() << std::endl;
	std::cout << "Message size: " << size << std::endl;
	if (msgVec[MSG_TYPE].to_ulong() == ERR_CODE) {
		std::string messageInfo;
		messageInfo.resize(size + END);
		for (int i = 0; i < size; i++) {
			messageInfo[i] = (char)msgVec[i + BASIC_LEN].to_ulong();
		}
		messageInfo[size] = 0;
		std::cout << "Message: " << messageInfo << std::endl;
	}
	else {
		std::string messageInfo;
		messageInfo.resize(size, 0);
		for (int i = 0; i < size; i++)
		{
			messageInfo[i] = (char)msgVec[i + sizeof(char) + sizeof(int)].to_ulong();
		}
		std::cout << "Message: " << messageInfo << std::endl;
	}
}

bool checkLoginRequestHandler() {
	SqliteDatabase db;
	RequestHandlerFactory b(&db);
	LoginRequestHandler *lrh = b.createLoginRequestHandler();
	Request rqst;
	rqst.id = SIGNUP;
	rqst.receivalTime = 0;
	std::string str("21111{ \"username\": \"ori\", \"password\": \"1234\", \"email\": \"orikoj10\" }");
	aVec vec;
	vec.resize(str.size(), 0);
	for (unsigned int i = 0; i < str.size(); i++) {
		vec[i] = str[i];
	}
	unsigned int fieldSize = (unsigned int)strlen("{ \"username\": \"ori\", \"password\": \"1234\", \"email\": \"orikoj10\" }");
	memcpy(vec.data() + DATA_LEN_BYTE, &fieldSize, sizeof(char) * DATA_LEN_BYTES);
	rqst.buffer = vec;
	if (lrh->isRequestRelevant(rqst))
	{
		RequestResult test = lrh->handleRequest(rqst);
		checkMsg(test.response);
	}
	delete lrh;
	return true;
}

