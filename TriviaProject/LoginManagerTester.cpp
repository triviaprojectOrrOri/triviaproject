#include "LoginManagerTester.h"


bool checkLoginmanager() {
	remove("TriviaDB.sqlite");//making sure that there is no database that currently exists, as it would ruin the test
	SqliteDatabase db;
	LoginManager lm(&db);
	lm.signup("Orr", "321123", "orr.sharon");
	try {
		lm.login("Orr", "321123");
		return false;
	}
	catch (std::exception) {}
	if (lm.isUserLoggedIn("Orr") != true)
		return false;
	lm.logout("Orr");
	if (lm.isUserLoggedIn("Orr") != false)
		return false;
	try {
		lm.signup("Orr", "555", "6");
		return false;
	}
	catch (std::exception) {}
	try {
		lm.login("Orr", "555");
		return false;
	}
	catch (std::exception) {}
	lm.login("Orr", "321123");
	if (lm.isUserLoggedIn("Orr") != true)
		return false;
	return true;
}
