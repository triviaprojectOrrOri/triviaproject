#include "Room.h"



Room::Room(RoomData metadata) : m_metadata(metadata)
{
}

Room::Room()
{
}


Room::~Room()
{
}

RoomData Room::getMetadata() const
{
	return m_metadata;
}

void Room::addUser(std::string username)
{
	if (m_users.size() >= m_metadata.maxPlayers) {
		throw std::exception("Error - too many players");
	}
	m_users.push_back(LoggedUser(username));
}

void Room::removeUser(std::string username)
{
	auto locUser = std::find(m_users.begin(), m_users.end(), LoggedUser(username));
	if (locUser != m_users.end()) {//if the username was found in the vector
		m_users.erase(locUser);
		return;
	}
	throw std::exception("Error - the user that was to be removed wasn't found");
}

std::vector<LoggedUser> Room::getAllUsers() const
{
	return m_users;
}

void Room::setRoomState(bool newState)
{
	m_metadata.isActive = newState;
}
