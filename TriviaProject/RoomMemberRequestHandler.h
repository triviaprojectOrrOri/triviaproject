#pragma once
#include "RequestHandlerFactory.h"
class RoomMemberRequestHandler : public IRequestHandler
{
private:
	roomID m_roomID;
	LoggedUser m_user;
	RoomManager *m_roomManager;
	RequestHandlerFactory *m_handlerFactory;
	RequestResult leaveRoom(Request rqst);//closes the admin's room
	RequestResult getRoomState(Request rqst);//starts the game
	void error(const char * errorMsg, RequestResult& toReturn);//in case of error
public:
	virtual bool isRequestRelevant(const Request& rqst) const override;
	virtual RequestResult handleRequest(const Request& rqst) override;
	virtual int getType() override;

	RoomMemberRequestHandler(LoggedUser user, roomID id, RoomManager* roomManager, RequestHandlerFactory* handlerFactory);
	~RoomMemberRequestHandler();
};

