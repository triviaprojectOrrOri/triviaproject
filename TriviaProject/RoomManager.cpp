#include "RoomManager.h"



RoomManager::RoomManager(): m_nextRoomNum(0)
{
}


RoomManager::~RoomManager()
{
}

int RoomManager::createRoom(LoggedUser admin, RoomData metadata)
{
	metadata.id = m_nextRoomNum;
	Room toAdd(metadata);
	toAdd.addUser(admin.getUsername());
	m_rooms.insert({ m_nextRoomNum, toAdd });
	return m_nextRoomNum++;
}

void RoomManager::deleteRoom(const roomID id)
{
	if (m_rooms.find(id) == m_rooms.end()) {
		throw std::exception("Error - room doesn't exist");
	}
	m_rooms.erase(id);
}

void RoomManager::joinRoom(LoggedUser user, roomID id)
{
	m_rooms[id].addUser(user.getUsername());
}

void RoomManager::removePlayer(LoggedUser user, roomID id)
{
	if (m_rooms.find(id) == m_rooms.end()) {
		throw std::exception("Error - room doesn't exist");
	}
	m_rooms[id].removeUser(user.getUsername());
}

unsigned int RoomManager::getRoomState(roomID ID)
{
	return m_rooms[ID].getMetadata().id;
}

std::vector<RoomData> RoomManager::getRooms() const
{
	std::vector<RoomData> toReturn;
	for (auto it = m_rooms.begin(); it != m_rooms.end(); it++) {
		toReturn.push_back(it->second.getMetadata());
	}
	return toReturn;
}

Room RoomManager::getRoomById(roomID roomId) const
{
	auto room = m_rooms.find((roomID)roomId);
	if (room == m_rooms.end()) {
		throw std::exception("Error - room doesn't exist");
	}
	return room->second;
}

void RoomManager::setRoomState(roomID ID, bool newState)
{
	m_rooms[ID].setRoomState(newState);
}
