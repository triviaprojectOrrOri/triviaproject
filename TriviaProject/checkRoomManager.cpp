#include "checkRoomManager.h"



checkRoomManager::checkRoomManager()
{
}


checkRoomManager::~checkRoomManager()
{
}

bool checkRoomManager::test()
{
	RoomManager toTest;
	LoggedUser user("Orr");
	RoomData data;
	data.questionsCount = 1;
	data.id = 0;
	data.isActive = false;
	data.maxPlayers = 5;
	data.name = "OrrRoom";
	data.timePerQuestion = 5;
	toTest.createRoom(user, data);
	if (toTest.getRoomState(0) != 0) {
		return false;
	}
	auto vec = toTest.getRooms();
	if (vec.size() != 1) {
		return false;
	}
	if (!(vec[0].id == 0 && vec[0].isActive == false && vec[0].maxPlayers == 5 && vec[0].name == "OrrRoom" && vec[0].timePerQuestion == 5)) {
		return false;
	}
	toTest.createRoom(user, data);
	vec = toTest.getRooms();
	if (vec.size() != 2) {
		return false;
	}
	toTest.deleteRoom(0);
	toTest.deleteRoom(1);
	try {
		toTest.deleteRoom(0);
		return false;
	}
	catch (std::exception e) {}
	return true;

}
