#pragma once
#include <winsock2.h>
#include <Windows.h>
#include <ws2tcpip.h>
#include <map>
#include <ctime>
#include <thread>
#include <mutex>
#include <iostream>
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")

#define NOT_TEST -1
#define DEFAULT_PORT "8008"
#define MAX_LEN 1024
#define CONNECTION_TERMINATED 10053


class Communicator
{
private:
	std::mutex clientsMtx;
	std::vector<std::thread*> threads;//threads connecting to all of the clients, only used when the number of connections is limited
	int m_numConnections;//number of connections to allow, if it is -1 (the default value) infinite connections are allowed
	std::map<SOCKET, IRequestHandler*> m_clients;
	RequestHandlerFactory m_handlerFactory;
	SOCKET m_listenSoc;
	//used as a thread, handle's each client
	void handleClient(SOCKET clientSoc);
	//starts handleClient and adds to threads if needed
	void startThreadForNewClient(SOCKET clientSoc);
	//sets up the listening socket
	void setUpListenSoc(WSAData);
	char * getClientMsg(SOCKET clientSoc, int& lenBuf);
	void sendMsg(SOCKET clientSoc, RequestResult res);
	//to call when the user is exiting
	void exitEverything(SOCKET clientSoc);
public:
	Communicator(IDatabase * db, int numConnections);
	~Communicator();
	//binds the listening socket
	void bindAndListen();
	//handles requests to start talking to the server, calls startThreadForNewClient
	void handleRequests();
	//getter
	int getNumToConnect() const;
};

