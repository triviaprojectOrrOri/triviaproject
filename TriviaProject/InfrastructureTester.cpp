#include "InfrastructureTester.h"



std::vector<char> InfrastructureTester::getMessageToSend()
{
	std::string toAdd;
	std::vector<char> toReturn;
	char lenChar[4];
	int len;
	toAdd += (char)std::string(USERNAME_TO_USE).length();
	toAdd += USERNAME_TO_USE;
	toAdd += (char)std::string(PASSWORD_TO_USE).length();
	toAdd += PASSWORD_TO_USE;
	toAdd += (char)std::string(EMAIL_TO_USE).length();
	toAdd += EMAIL_TO_USE;
	toAdd += (char)0;
	len = toAdd.length();
	memcpy(lenChar, &len, sizeof(int));
	toReturn.push_back((char)TYPE_MSG);
	for (int i = 0; i < 4; i++) {
		toReturn.push_back(lenChar[i]);
	}
	for (auto it = toAdd.begin(); it != toAdd.end(); it++) {
		toReturn.push_back(*it);
	}
	return toReturn;
}

bool InfrastructureTester::connectToServer()
{
	WSADATA wsaData;
	char buffer[MAX_LEN];
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;
	std::vector<char> toSendVec = getMessageToSend();
	char * toSendChar = new char[toSendVec.size()];
	for (unsigned int i = 0; i < toSendVec.size(); i++) {
		toSendChar[i] = toSendVec[i];
	}
	int iResult;
	int recvbuflen = MAX_LEN;


	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		delete toSendChar;
		return false;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	// Resolve the server address and port
	iResult = getaddrinfo(LOCAL_HOST, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		delete toSendChar;
		return false;
	}

	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			delete toSendChar;
			return false;
		}

		// Connect to server.
		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		delete toSendChar;
		return false;
	}

	// Send an initial buffer
	iResult = send(ConnectSocket, toSendChar, (int)toSendVec.size(), 0);
	if (iResult == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		delete toSendChar;
		return false;
	}
	int len = recv(ConnectSocket, buffer, MAX_LEN, 0);
	std::vector<Byte> toPrint(buffer, buffer + len);
	TestResponse::printMsg(toPrint);


	// shutdown the connection since no more data will be sent
	iResult = shutdown(ConnectSocket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(ConnectSocket);
		delete toSendChar;
		return false;
	}
	// cleanup
	closesocket(ConnectSocket);
	delete toSendChar;
	return ((bool)buffer[0] == EXPECTED_RESULT);
}

InfrastructureTester::InfrastructureTester()
{
}


InfrastructureTester::~InfrastructureTester()
{
}

bool InfrastructureTester::test()
{
	SqliteDatabase db;
	Server s(&db, 2);
	std::thread t(&Server::run, &s);
	std::thread a(&InfrastructureTester::connectToServer, this);
	std::thread b(&InfrastructureTester::connectToServer, this);
	t.join();
	a.join();
	b.join();
	return true;
}
