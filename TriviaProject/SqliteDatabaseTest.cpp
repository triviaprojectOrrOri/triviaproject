#pragma once
#include <iostream>
#include "SqliteDatabase.h"

bool checkSqliteDatabase() {
	SqliteDatabase a;
	std::vector<std::string> ans;
	for (int i = 0; i < 4; i++) {
		ans.push_back(std::to_string(i * 5));
	}
	a.addQuestion("40", ans, 2);
	std::cout << (a.doesUserExist("Orr") == true) << (a.doesUserExist("O") == false) << std::endl;
	auto b = a.getHighScores();
	std::cout << b[LoggedUser("Orr")] << " " << b[LoggedUser("Bob")] << std::endl;
	auto questions = a.getQuestion(1);
	a.addGame(5, "NOW", "NOW");
	return true;
}
