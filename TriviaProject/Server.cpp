#include "Server.h"



Server::Server(IDatabase * db, int test): m_database(db), m_handlerFactory(db)
{
	m_communicator = new Communicator(db, test);
}


Server::~Server()
{
	delete m_communicator;
}

void Server::run()
{
	int timesConnected = 0;
	m_communicator->bindAndListen();
	while (m_communicator->getNumToConnect() == NOT_TEST || timesConnected < m_communicator->getNumToConnect() ) {
		m_communicator->handleRequests();
		timesConnected++;
	}
}
