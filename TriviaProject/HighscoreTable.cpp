#include "HighscoreTable.h"



HighscoreTable::HighscoreTable(IDatabase * db): m_database(db)
{
}


HighscoreTable::~HighscoreTable()
{
}

std::map<LoggedUser,int> HighscoreTable::getHighscores()
{
	return m_database->getHighScores();
}
