#pragma once
#include <iostream>
#include <memory>
#include <string>
#include <bitset>
#include <stdio.h>
#include "json.hpp"
#define CODE_END 1
#define DATALEN_LEN 4
#define FIELDLEN_LEN 1
#define LOGIN_FIELDS 2
#define SIGNUP_FIELDS 3
#define USERNAME 0
#define PASSWORD 1
#define CREATE_ROOM_INT_FIELDS 3
#define MAX_USERS 0
#define QUESTION_COUNT 1
typedef std::vector<std::bitset<8>> aVec;
typedef struct LoginRequest{
	std::string username;
	std::string password;
}LoginRequest;
typedef struct SignupRequest{
	std::string username;
	std::string password;
	std::string email;
}SignupRequest;
//V2
typedef struct JoinRoomRequest{
	unsigned int roomId;
}JoinRoomRequest;

typedef struct GetPlayersInRoomRequest {
	unsigned int roomId;
}GetPlayersInRoomRequest;

typedef struct CreateRoomRequest {
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
}CreateRoomRequest;

class JsonRequestPacketDeserializer
{
	static std::string extractField(const std::string& msg);//this function recieves data(buffer without code and data length), with the first char saying how many bytes extract, extracts those chars and returns them.
	static std::string extractData(const aVec& data);//this function recieves buffer, with the first 4 chars saying how many bytes extract, extracts those chars and returns them.
	//V2
	static unsigned int extractUnsignedInt(const std::string& msg);//this function recieves data(buffer without code and data length), with the first char saying how many bytes extract, extracts those chars and returns them.
public:
	static LoginRequest deserializeLoginRequest(const aVec& buffer);
	static SignupRequest deserializeSignupRequest(const aVec& buffer);
	//V2
	static GetPlayersInRoomRequest deserializeGetPlayersRequest(const aVec& buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(const aVec& buffer);
	static CreateRoomRequest deserializeCreateRoomReuquest(const aVec& buffer);//The data parti n this this has 1 bytes contaning the length of the string, and then 3 times 4 bytes containing the ints
};
void printPtr(const aVec & ptr, const unsigned int len);
aVec slice(const aVec& str, const unsigned int start, const unsigned int len);//performs str slicing and returns a unique ptr
