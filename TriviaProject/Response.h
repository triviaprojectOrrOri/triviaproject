#pragma once
#include <memory>
#include <string>
#include <vector>
#include <bitset>
#include <map>
#include "json.hpp"
#include "Room.h"

typedef std::bitset<8> Byte;


#define BASIC_LEN 1 + sizeof(int)
#define MSG_LEN 4
#define SKIP_TYPE 1
#define MSG_TYPE 0
#define END 1
#define SQUIGLY_COLON 3
#define STR_QUOTES 4
#define INT_QUOTES 2

enum codes {
	ERR_CODE,
	LOGIN_CODE,
	SIGNUP_CODE,
	LOGOUT_CODE,
	CREATE_ROOM_CODE,
	JOIN_ROOM_CODE,
	GET_PLAYERS_IN_ROOM_CODE,
	GET_ROOMS_CODE,
	HIGHSCORE_CODE,
	START_GAME_CODE, 
	GET_ROOM_STATE_CODE,
	LEAVE_ROOM_CODE,
	CLOSE_ROOM_CODE
};



typedef struct Highscore {
	unsigned int score;
	std::string username;
} Highscore;

typedef struct LeaveRoomResponse {
	unsigned int status;
} LeaveRoomResponse;

typedef struct GetRoomStateResponse {
	unsigned int status;
	bool hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	unsigned int answerTimeout;
} GetRoomStateResponse;

typedef struct StartGameResponse {
	unsigned int status;
} StartGameResponse;

typedef struct CloseRoomResponse {
	unsigned int status;
} CloseRoomResponse;

typedef struct LogoutResponse {
	unsigned int status;
} LogoutResponse;

typedef struct GetRoomsResponse {
	unsigned int status;
	std::vector<RoomData> rooms;
} GetRoomsResponse;

typedef struct GetPlayersInRoomResponse {
	std::vector<std::string> players;
} GetPlayersInRoomResponse;

typedef struct HighscoreResponse {
	unsigned int status;
	std::vector<Highscore> highscores;
} HighscoreResponse;

typedef struct JoinRoomResponse {
	unsigned int status;
} JoinRoomResponse;

typedef struct CreateRoomResponse {
	unsigned int status;
} CreateRoomResponse;

typedef struct LoginResponse {
	unsigned int status;
} LoginResponse;

typedef struct SignupResponse {
	unsigned int status;
} SignupResponse;

typedef struct ErrorResponse {
	std::string message;
} ErrorResponse;

class JsonResponsePacketSerializer {
public:
	static std::vector<Byte> serializeResponse(ErrorResponse);
	static std::vector<Byte> serializeResponse(LoginResponse);
	static std::vector<Byte> serializeResponse(SignupResponse);
	static std::vector<Byte> serializeResponse(LogoutResponse);
	static std::vector<Byte> serializeResponse(CreateRoomResponse);
	static std::vector<Byte> serializeResponse(JoinRoomResponse);
	static std::vector<Byte> serializeResponse(GetPlayersInRoomResponse);
	static std::vector<Byte> serializeResponse(GetRoomsResponse);
	static std::vector<Byte> serializeResponse(HighscoreResponse);
	static std::vector<Byte> serializeResponse(CloseRoomResponse);
	static std::vector<Byte> serializeResponse(StartGameResponse);
	static std::vector<Byte> serializeResponse(LeaveRoomResponse);
	static std::vector<Byte> serializeResponse(GetRoomStateResponse);
private:
	static std::vector<Byte> serializeResponse(std::map<std::string, std::string>, int responseType);//serializes the response that is translated from the original response gotten
	static std::string createJsonDict(std::map<std::string, std::string>);//creates a json formatted dict from the map's contents

	template <class T>
	static std::string makeVecToStr(std::vector<T>);//turns the vector into a string

	static std::string convertToStr(std::string);//convertors from types to strings
	static std::string convertToStr(Highscore);
	static std::string convertToStr(RoomData);
};