#include "SqliteDatabase.h"


//opens the database, and if it doesn't exist yet creates it
bool SqliteDatabase::open()
{
	int doesFileExist = _access("TriviaDB.sqlite", 0);
	std::unique_lock<std::mutex> dbLock(this->openDB);
	int res = sqlite3_open("TriviaDB.sqlite", &m_db);
	dbLock.unlock();
	char * errMessage = nullptr;
	if (res != SQLITE_OK) {
		m_db = nullptr;
		return false;
	}
	if (NOT_EXIST == doesFileExist) {
		const char* msg[NUM_TABLES];
		msg[TURN_ON_FOREIGN_KEY] = "PRAGMA foreign_keys = ON;";
		msg[USER] = "CREATE TABLE User( \
				  username TEXT PRIMARY KEY, \
				  password TEXT NOT NULL, \
				  email TEXT NOT NULL);";
		msg[QUESTION] = "CREATE TABLE Question( \
					  question_id INTEGER PRIMARY KEY AUTOINCREMENT, \
					  question TEXT NOT NULL, \
					  correct_ans TEXT NOT NULL, \
					  ans2 TEXT NOT NULL, \
					  ans3 TEXT NOT NULL, \
					  ans4 TEXT NOT NULL);";
		msg[GAME] = "CREATE TABLE Game( \
						 game_id INTEGER PRIMARY KEY AUTOINCREMENT, \
						 status INTEGER NOT NULL, \
						 start_time DATETIME NOT NULL, \
						 end_time DATETIME NOT NULL); ";
		msg[HIGH_SCORES] = "CREATE TABLE Highscores( \
								highscore_id INTEGER PRIMARY KEY AUTOINCREMENT, \
								score INTEGER NOT NULL, \
								user TEXT NOT NULL, \
								FOREIGN KEY (user) REFERENCES User(username));";
		std::unique_lock<std::mutex> dbLock(this->openDB);
		for (int i = 0; i < NUM_TABLES; i++) {//executing all the lines
			res = sqlite3_exec(m_db, msg[i], nullptr, nullptr, &errMessage);
			if (res != SQLITE_OK) {
				std::string toThrow(errMessage);
				sqlite3_free(errMessage);
				throw std::exception(toThrow.c_str());
			}
		}
		dbLock.unlock();
	}
	return true;
}

int callback(void * data, int argc, char ** argv, char ** azColName)//the callback function to be used when calling sqlite3_exec
{
	std::vector<std::vector<std::string>>* cols = static_cast<std::vector<std::vector<std::string>>*>(data);
	int changed = 0, num_line = cols->size(), entered_in = 0;
	std::vector<std::string> remember = (*cols)[0];//remembering the column names to get
	cols->push_back(remember);
	for (int i = 0; i < argc; i++) {
		for (unsigned int j = 0; j < (*cols)[num_line].size(); j++) {//checking if the column name is equal to the one entered
			if ((*cols)[0][j].compare(azColName[i]) == EQUAL) {
				if (argv[i] == NULL) {
					break;
				}
				(*cols)[num_line][j] = argv[i];//adding the argument
				changed++;
				entered_in++;
			}
		}
		if (entered_in == (*cols)[0].size()) {//if no arguments were entered into the vector
			entered_in = 0;
			num_line++;
			cols->push_back(remember);
		}
	}
	cols->pop_back();
	return 0;
}

std::vector<std::vector<std::string>> SqliteDatabase::callbackUser(const char * command, std::vector < std::vector<std::string>> insertTo) { //uses sqlite3_exe with callback, and inserts it into insertTo
	char * errMessage = nullptr;
	std::unique_lock<std::mutex> dbLock(this->openDB);
	int res = sqlite3_exec(m_db, command,
		callback, &insertTo, &errMessage);
	if (res != SQLITE_OK) {
		std::string toThrow(errMessage);
		sqlite3_free(errMessage);
		throw std::exception(toThrow.c_str());
	}
	dbLock.unlock();
	insertTo.erase(insertTo.begin());//erasing the starting value that were added to specify which columns to get
	return insertTo;
}

SqliteDatabase::SqliteDatabase(): m_db(nullptr)
{
	eng = std::mt19937(rd());//making it to that eng is random
	if (!open()) {
		throw std::exception("Error - couldn't open database");
	}
}


SqliteDatabase::~SqliteDatabase()
{
	sqlite3_close(m_db);
}

//getting the highscores and putting them in the map;
std::map<LoggedUser, int> SqliteDatabase::getHighScores()
{
	std::vector<std::vector<std::string>> highScores;
	std::map<LoggedUser, int> toReturn;
	highScores.push_back(std::vector<std::string>());
	highScores[0].push_back("score");//adding the columns to search for
	highScores[0].push_back("user");
	highScores = callbackUser("SELECT score, user FROM Highscores;", highScores);
	for (unsigned int i = 0; i < highScores.size(); i++) {
		LoggedUser a(highScores[i][NAME]);
		toReturn.insert(std::pair<LoggedUser, int>(LoggedUser(highScores[i][NAME]), std::stoi(highScores[i][SCORE])));
	}
	return toReturn;
}

bool SqliteDatabase::doesUserExist(std::string user)
{
	std::vector < std::vector< std::string> > colNames;
	colNames.push_back(std::vector<std::string>());
	colNames[0].push_back("username");//adding the column to search for
	return !callbackUser((std::string("SELECT username FROM User WHERE username = '") + user + "';").c_str(), colNames).empty();//if the returned vector isn't empty, meaning that the username was found
}

bool SqliteDatabase::isPasswordCorrect(std::string username, std::string password)
{
	std::vector < std::vector< std::string> > colNames;
	std::hash<std::string> strHash;
	colNames.push_back(std::vector<std::string>());
	colNames[0].push_back("username");//adding the column to search for
	return !callbackUser((std::string("SELECT username FROM User WHERE username = '") + username + "' AND password = '" +std::to_string(strHash(password)) + "';").c_str(), colNames).empty();//if the returned vector isn't empty, meaning that the username was found
	return true;
}

std::list<Question> SqliteDatabase::getQuestion(int numQuestions)
{
	std::list<Question> questions;
	std::vector<std::vector<std::string>> highScores;
	highScores.push_back(std::vector < std::string>());
	highScores[0].push_back("question");//adding the columns to search for
	highScores[0].push_back("correct_ans");
	highScores[0].push_back("ans2");
	highScores[0].push_back("ans3");
	highScores[0].push_back("ans4");
	std::set<int> questionsToGet;
	highScores = callbackUser("SELECT question, correct_ans, ans2, ans3, ans4 FROM Question;", highScores);//getting all the questions, later picking random ones
	if (highScores.size() <(unsigned int)numQuestions) {
		throw std::exception("Error - not enough questions in database");
	}
	distr = std::uniform_int_distribution<>(0, highScores.size() - START_AT_ZERO);
	while (questionsToGet.size() != numQuestions) {//getting the amount of questions requested randomly 
		questionsToGet.insert(distr(eng));
	}
	for (auto it = questionsToGet.begin(); it != questionsToGet.end(); it++) {
		distr = std::uniform_int_distribution<>(0, 3);
		std::vector<std::string> answers;
		std::vector<int> repeat;
		int correct;
		for (int i = 0; i < 4; i++) {//getting questions in random order
			int inTable;
			do {
				inTable = distr(eng);
			} while (std::find(repeat.begin(), repeat.end(), inTable) != repeat.end());
			repeat.push_back(inTable);
			if (CORRECT_ANSWER == i) {//if the answer is correct, save it
				correct = inTable;
			}
			answers.push_back(highScores[*it][inTable]);
		}
		questions.push_back(Question(highScores[*it][QUESTION_POS], answers, correct));
	}
	return questions;
}

void SqliteDatabase::addUser(std::string username, std::string password, std::string email)//adds a user to the database
{
	char *errMessage = nullptr;
	std::hash<std::string> strHash;
	std::unique_lock<std::mutex> dbLock(this->openDB);
	int res = sqlite3_exec(m_db, (std::string("INSERT INTO User (username, password, email) VALUES ('") +
									username + "', '" + std::to_string(strHash(password)) + "', '" + email + "');").c_str()
		, nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::string toThrow(errMessage);
		sqlite3_free(errMessage);
		throw std::exception(toThrow.c_str());
	}
	dbLock.unlock();
}

void SqliteDatabase::addQuestion(std::string question, std::vector<std::string> answers, int correct_ans)//adds a question
{
	char *errMessage = nullptr;
	std::string command = std::string("INSERT INTO Question (question, correct_ans, ans2, ans3, ans4) VALUES ('") +
		question + "', '" + answers[CORRECT_ANSWER] + "', '" + answers[OTHER_QUESTION2] + "', '" + answers[OTHER_QUESTION3] + "', '" + answers[OTHER_QUESTION4] + "');";
	std::unique_lock<std::mutex> dbLock(this->openDB);
	int res = sqlite3_exec(m_db, command.c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::string toThrow(errMessage);
		sqlite3_free(errMessage);
		throw std::exception(toThrow.c_str());
	}
	dbLock.unlock();
}

void SqliteDatabase::addGame(int status, std::string start, std::string end)//adds a game
{
	char *errMessage = nullptr;
	std::unique_lock<std::mutex> dbLock(this->openDB);
	int res = sqlite3_exec(m_db, (std::string("INSERT INTO Game (status, start_time, end_time) VALUES ('") +
		std::to_string(status) +"', '" + start + "', '" + end + "');").c_str()
		, nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::string toThrow(errMessage);
		sqlite3_free(errMessage);
		throw std::exception(toThrow.c_str());
	}
	dbLock.unlock();
}

void SqliteDatabase::addHighScore(int score, std::string user)//adds a high score
{
	char *errMessage = nullptr;
	std::unique_lock<std::mutex> dbLock(this->openDB);
	int res = sqlite3_exec(m_db, (std::string("INSERT INTO Highscores (score, user) VALUES ('") +
		std::to_string(score) + "', '" + user + "');").c_str()
		, nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK) {
		std::string toThrow(errMessage);
		sqlite3_free(errMessage);
		throw std::exception(toThrow.c_str());
	}
	dbLock.unlock();
}
