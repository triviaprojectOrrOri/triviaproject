#include "RoomAdminRequestHandler.h"


RoomAdminRequestHandler::RoomAdminRequestHandler(LoggedUser user, roomID id, RoomManager * roomManager, RequestHandlerFactory * handlerFactory) :m_roomManager(roomManager), m_handlerFactory(handlerFactory), m_user(user), m_roomID(id)
{
}

RoomAdminRequestHandler::~RoomAdminRequestHandler()
{
}

bool RoomAdminRequestHandler::isRequestRelevant(const Request & rqst) const
{
	if (rqst.id >= CLOSE_OR_LEAVE_ROOM && rqst.id <= START_GAME) {
		return true;
	}
	return false;
}

RequestResult RoomAdminRequestHandler::handleRequest(const Request & rqst)
{
	switch (rqst.id) {
	case CLOSE_OR_LEAVE_ROOM:
		return closeRoom(rqst);
	case START_GAME:
		return startGame(rqst);
	case GET_ROOM_STATE:
		return getRoomState(rqst);
	default:
		//if the request isn't of any of the possible types
		RequestResult toReturn;
		toReturn.newHandle = nullptr;
		ErrorResponse errRes;
		errRes.message = "Error - request code not recognized";
		toReturn.response = JsonResponsePacketSerializer::serializeResponse(errRes);
		return toReturn;
	}
}

int RoomAdminRequestHandler::getType()
{
	return ROOM_ADMIN_REQUEST_HANDLER;
}

RequestResult RoomAdminRequestHandler::closeRoom(Request rqst)
{
	RequestResult result;
	try {
		CloseRoomResponse response;
		m_roomManager->deleteRoom(m_roomID);
		result.newHandle = m_handlerFactory->createMenuRequestHandler(m_user.getUsername());
		response.status = true;
		result.response = JsonResponsePacketSerializer::serializeResponse(response);
	}
	catch (std::exception e) {
		error(e.what(), result);
	}
	return result;
}

RequestResult RoomAdminRequestHandler::startGame(Request rqst)
{
	RequestResult result;
	try {
		StartGameResponse response;
		m_roomManager->setRoomState(m_roomID, true);
		result.newHandle = nullptr;//will be GameRequestHandler
		response.status = true;
		result.response = JsonResponsePacketSerializer::serializeResponse(response);
	}
	catch (std::exception e) {
		error(e.what(), result);
	}
	return result;
}

void RoomAdminRequestHandler::error(const char * errorMsg, RequestResult & toReturn)
{
	toReturn.newHandle = m_handlerFactory->createRoomAdminRequestHandler(m_user.getUsername(), m_roomID);
	ErrorResponse errRes = { errorMsg };
	toReturn.response = JsonResponsePacketSerializer::serializeResponse(errRes);
}

RequestResult RoomAdminRequestHandler::getRoomState(Request rqst)
{
	RequestResult result;
	try {
		GetRoomStateResponse response;
		Room roomToGet = m_roomManager->getRoomById(m_roomID);
		RoomData metadata = roomToGet.getMetadata();
		std::vector<LoggedUser> loggedUsers = roomToGet.getAllUsers();
		result.newHandle = m_handlerFactory->createRoomAdminRequestHandler(m_user.getUsername(), m_roomID);

		response.status = true;
		response.answerTimeout = metadata.timePerQuestion;
		response.hasGameBegun = metadata.isActive;
		response.questionCount = metadata.questionsCount;

		response.players;
		for (auto it = loggedUsers.begin(); it != loggedUsers.end(); it++) {
			response.players.push_back(it->getUsername());
		}
		result.response = JsonResponsePacketSerializer::serializeResponse(response);
	}
	catch (std::exception e) {
		error(e.what(), result);
	}
	return result;
}
