#include "LoginManager.h"

std::mutex LoginManager::changeLoggedInMtx;


LoginManager::LoginManager(IDatabase * database) : m_database(database)
{
}

LoginManager::~LoginManager()
{
}

void LoginManager::signup(std::string username, std::string password, std::string email)
{
	std::unique_lock<std::mutex> changeLoggedInLock(changeLoggedInMtx);
	if (this->m_database->doesUserExist(username))
		throw std::exception("Error - username already exists");
	this->m_database->addUser(username, password, email);
	this->m_loggedUsers.push_back(LoggedUser(username));//adds the user to the active users list
}

void LoginManager::login(std::string username, std::string password)
{
	std::unique_lock<std::mutex> changeLoggedInLock(changeLoggedInMtx);
	if (this->m_database->isPasswordCorrect(username, password)) {//if the password is correct
		if (!isUserLoggedIn(username)) { //if the user isn't already logged in
			this->m_loggedUsers.push_back(LoggedUser(username));//adds the user to the active users list
		}
		else
			throw std::exception("Error - user was already logged in");
	}
	else
		throw std::exception("Error - incorrect username or password");
}

void LoginManager::logout(std::string username)
{
	std::unique_lock<std::mutex> changeLoggedInLock(changeLoggedInMtx);
	auto toDelete = std::find(m_loggedUsers.begin(), m_loggedUsers.end(), username);//finds the pos of the user in m_loggedUsers
	if (toDelete == m_loggedUsers.end())
		throw std::exception("Error - user not found");
	m_loggedUsers.erase(toDelete);
}

bool LoginManager::isUserLoggedIn(std::string username) const
{
	return std::find(m_loggedUsers.begin(), m_loggedUsers.end(), LoggedUser(username)) != m_loggedUsers.end();
}
