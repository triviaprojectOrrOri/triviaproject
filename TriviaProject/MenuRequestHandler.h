#pragma once
#include "IRequestHandler.h"
#include "RoomManager.h"
#include "HighscoreTable.h"
#include "RequestHandlerFactory.h"
#include "JsonRequestPacketDeserializer.h"
#include "Response.h"
#include <string>
enum {
	SIGNOUT = 3,
	CREATE_ROOM,
	JOIN_ROOM,
	GET_PLAYERS_IN_ROOM,
	GET_ROOMS,
	GET_HIGH_SCORES 
};

class RequestHandlerFactory;

class MenuRequestHandler: public IRequestHandler
{
	LoggedUser m_user;
	RoomManager* m_roomManager;
	HighscoreTable* m_highscoreTable;
	RequestHandlerFactory* m_handlerFactory;

	RequestResult signout(const Request& rqst);
	RequestResult getRooms();
	RequestResult getPlayersInRoom(const Request& rqst);
	RequestResult getHighscores();
	RequestResult joinRoom(const Request& rqst);
	RequestResult createRoom(const Request& rqst);
	void error(const char * errorMsg, RequestResult& toReturn);//in case of error
public:
	virtual bool isRequestRelevant(const Request& rqst) const override;
	virtual RequestResult handleRequest(const Request& rqst) override;
	virtual int getType() override;
	MenuRequestHandler(LoggedUser user, RoomManager* roomManager, HighscoreTable* highscoreTable, RequestHandlerFactory* handlerFactory);

};

