#pragma once
#include "IDatabase.h"
#include "Communicator.h"
#include "RequestHandlerFactory.h"

#define STOP 2

class Server
{
	IDatabase* m_database;
	Communicator* m_communicator;
	RequestHandlerFactory m_handlerFactory;
public:
	Server(IDatabase * db, int test = NOT_TEST);
	~Server();
	void run();
};

