#include "LoginRequestHandler.h"

bool LoginRequestHandler::isRequestRelevant(const Request& rqst) const
{
	if (rqst.id == LOGIN || rqst.id == SIGNUP) {
		return true;
	}
	return false;
}

RequestResult LoginRequestHandler::handleRequest(const Request& rqst)
{
	if (rqst.id == LOGIN) {
		return this->login(rqst);
	}
	else if (rqst.id == SIGNUP) {
		return this->signup(rqst);
	}
	//if the request isn't of type SIGNUP or of type LOGIN
	RequestResult toReturn; 
	toReturn.newHandle = nullptr;
	ErrorResponse errRes;
	errRes.message = "Error - request code not recognized";
	toReturn.response = JsonResponsePacketSerializer::serializeResponse(errRes);
	return toReturn;
}

int LoginRequestHandler::getType()
{
	return LOGIN_REQUEST_HANDLER;
}

RequestResult LoginRequestHandler::login(const Request& rqst)
{
	std::string username, password;
	RequestResult toReturn;
	LoginRequest translatedRqst = JsonRequestPacketDeserializer::deserializeLoginRequest(rqst.buffer);//gets the translated request
	username = translatedRqst.username, password = translatedRqst.password;
	
	try {
		m_loginManager->login(username, password);
		toReturn.newHandle = m_handlerFactory->createMenuRequestHandler(username); //the new request handler
		LoginResponse logRes;
		logRes.status = true;
		toReturn.response = JsonResponsePacketSerializer::serializeResponse(logRes);
	}
	catch (std::exception e) {//if an error occured
		error(e.what(), toReturn);
	}
	return toReturn;
}

RequestResult LoginRequestHandler::signup(const Request& rqst)
{
	std::string username, password, email;
	RequestResult toReturn;
	SignupRequest translatedRqst = JsonRequestPacketDeserializer::deserializeSignupRequest(rqst.buffer);
	username = translatedRqst.username, password = translatedRqst.password, email = translatedRqst.email;

	try {
		m_loginManager->signup(username, password, email);
		toReturn.newHandle = m_handlerFactory->createMenuRequestHandler(username); //the new request handler
		SignupResponse signRes;
		signRes.status = true;
		toReturn.response = JsonResponsePacketSerializer::serializeResponse(signRes);
	}
	catch (std::exception e) {//if an error occured
		error(e.what(), toReturn);
	}
	return toReturn;
}

void LoginRequestHandler::error(const char * errorMsg, RequestResult & toReturn)
{
	toReturn.newHandle = m_handlerFactory->createLoginRequestHandler();
	ErrorResponse errRes = { errorMsg };
	toReturn.response = JsonResponsePacketSerializer::serializeResponse(errRes);
}

LoginRequestHandler::LoginRequestHandler(LoginManager* loginManager, RequestHandlerFactory* handlerFactory) :m_handlerFactory(handlerFactory), m_loginManager(loginManager)
{
}


LoginRequestHandler::~LoginRequestHandler()
{
}
