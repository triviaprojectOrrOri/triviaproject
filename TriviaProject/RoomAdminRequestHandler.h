#pragma once
#include "RequestHandlerFactory.h"

enum {
	CLOSE_OR_LEAVE_ROOM = 9,
	GET_ROOM_STATE,
	START_GAME
};

class RoomAdminRequestHandler: public IRequestHandler
{
private:
	roomID m_roomID;
	LoggedUser m_user;
	RoomManager *m_roomManager;
	RequestHandlerFactory *m_handlerFactory;
	RequestResult closeRoom(Request rqst);//closes the admin's room
	RequestResult startGame(Request rqst);//starts the game
	RequestResult getRoomState(Request rqst);//gets the state of the room
	void error(const char * errorMsg, RequestResult& toReturn);//in case of error
public:
	RoomAdminRequestHandler(LoggedUser user, roomID id, RoomManager* roomManager, RequestHandlerFactory* handlerFactory);
	~RoomAdminRequestHandler();
	virtual bool isRequestRelevant(const Request& rqst) const override;
	virtual RequestResult handleRequest(const Request& rqst) override;
	virtual int getType() override;
};

