#include <iostream>
#include "ResponseTest.h"


void TestResponse::printMsg(std::vector<Byte> msg) {
	int size;
	memcpy(&size, msg.data() + SKIP_TYPE, sizeof(int));
	std::cout << "Message type: " << msg[MSG_TYPE].to_ulong() << std::endl;

	std::cout << "Message size: " << size << std::endl;
	std::unique_ptr<char[]> messageInfo(new char[size + END]);
	for (int i = 0; i < size; i++) {
		messageInfo[i] = (char)msg[i + BASIC_LEN].to_ulong();
	}
	messageInfo[size] = 0;
	std::cout << "Message: " << messageInfo.get() << std::endl;
}

void TestResponse::checkResponse() {
	ErrorResponse a;
	LoginResponse b;
	SignupResponse c;
	LogoutResponse d;
	GetRoomsResponse e;
	GetPlayersInRoomResponse f;
	HighscoreResponse g;
	JoinRoomResponse h;
	CreateRoomResponse i;
	StartGameResponse l;
	LeaveRoomResponse m;
	CloseRoomResponse n;
	GetRoomStateResponse o;
	RoomData j;
	Highscore k;
	a.message = "HI";
	b.status = 1;
	c.status = 0;
	d.status = 1;
	e.status = 1;
	e.rooms;
	j.id = 0;
	j.isActive = true;
	j.maxPlayers = 5;
	j.name = "Room1";
	j.timePerQuestion = 5;
	e.rooms.push_back(j);
	e.rooms.push_back(j);
	e.rooms.push_back(j);
	j.name = "Room2";
	e.rooms.push_back(j);
	k.score = 5;
	k.username = "Orr";
	f.players;
	f.players.push_back("Orr");
	f.players.push_back("Ori");
	g.highscores;
	g.highscores.push_back(k);
	g.highscores.push_back(k);
	g.highscores.push_back(k);
	g.status = 1;
	h.status = 1;
	i.status = 0;
	l.status = 1;
	m.status = 0;
	n.status = 1;
	o.status = 0;
	o.questionCount = 5;
	o.answerTimeout = 5;
	o.hasGameBegun = 1;
	o.players;
	o.players.push_back("Orr");
	o.players.push_back("Ori");
	o.players.push_back("Maayan");
	
	printMsg(JsonResponsePacketSerializer::serializeResponse(a));
	printMsg(JsonResponsePacketSerializer::serializeResponse(b));
	printMsg(JsonResponsePacketSerializer::serializeResponse(c));
	printMsg(JsonResponsePacketSerializer::serializeResponse(d));
	printMsg(JsonResponsePacketSerializer::serializeResponse(e));
	printMsg(JsonResponsePacketSerializer::serializeResponse(f));
	printMsg(JsonResponsePacketSerializer::serializeResponse(g));
	printMsg(JsonResponsePacketSerializer::serializeResponse(h));
	printMsg(JsonResponsePacketSerializer::serializeResponse(i));
	printMsg(JsonResponsePacketSerializer::serializeResponse(l));
	printMsg(JsonResponsePacketSerializer::serializeResponse(m));
	printMsg(JsonResponsePacketSerializer::serializeResponse(n));
	printMsg(JsonResponsePacketSerializer::serializeResponse(o));
}
