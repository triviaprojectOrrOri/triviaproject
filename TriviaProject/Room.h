#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include <exception>
#include "LoggedUser.h"

typedef struct RoomData {
	std::string name;
	unsigned int id;
	unsigned int questionsCount;
	unsigned int maxPlayers;
	unsigned int timePerQuestion;
	bool isActive;
} RoomData;

class Room
{
private:
	RoomData m_metadata;
	std::vector<LoggedUser> m_users;
public:
	Room(RoomData);
	Room();
	~Room();
	RoomData getMetadata() const;
	void addUser(std::string username);
	void removeUser(std::string username);
	std::vector<LoggedUser> getAllUsers() const;//getter
	void setRoomState(bool newState);//setter
};

