#include "MenuRequestHandler.h"



RequestResult MenuRequestHandler::signout(const Request & rqst)
{
	RequestResult result;
	try {
		LogoutResponse response;
		(m_handlerFactory->getLoginManager())->logout(m_user.getUsername());
		result.newHandle = m_handlerFactory->createLoginRequestHandler();
		response.status = true;
		result.response = JsonResponsePacketSerializer::serializeResponse(response);
	}
	catch (std::exception e) {
		error(e.what(), result);
	}
	return result;
}

RequestResult MenuRequestHandler::getRooms()
{
	RequestResult result;
	try	{
		GetRoomsResponse response;
		response.rooms = m_roomManager->getRooms();
		result.newHandle = m_handlerFactory->createMenuRequestHandler(m_user.getUsername());
		response.status = true;
		result.response = JsonResponsePacketSerializer::serializeResponse(response);
	}
	catch (std::exception e){
		error(e.what(), result);
	}
	return result;
}

RequestResult MenuRequestHandler::getPlayersInRoom(const Request & rqst)
{
	RequestResult result;
	GetPlayersInRoomResponse response;
	GetPlayersInRoomRequest deserializedRequest = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(rqst.buffer);

	std::vector<LoggedUser> loggedUsersVec = m_roomManager->getRoomById(deserializedRequest.roomId).getAllUsers();
	response.players.resize(loggedUsersVec.size());
	for (unsigned int i = 0; i < loggedUsersVec.size(); i++)
	{
		response.players[i] = loggedUsersVec[i].getUsername();
	}
	result.newHandle = m_handlerFactory->createMenuRequestHandler(m_user.getUsername());
	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}

RequestResult MenuRequestHandler::getHighscores()
{
	RequestResult result;
	try {
		HighscoreResponse response;
		std::map<LoggedUser, int> highscoresMap = m_highscoreTable->getHighscores();
		for (auto highscoresIter = highscoresMap.begin(); highscoresIter != highscoresMap.end(); highscoresIter++){
			Highscore tempHighscore;
			tempHighscore.score = highscoresIter->second;
			tempHighscore.username = highscoresIter->first.getUsername();
			response.highscores.push_back(tempHighscore);
		}
		result.newHandle = m_handlerFactory->createMenuRequestHandler(m_user.getUsername());
		response.status = true;
		result.response = JsonResponsePacketSerializer::serializeResponse(response);
	}
	catch (std::exception e) {
		error(e.what(), result);
	}
	return result;
}

RequestResult MenuRequestHandler::joinRoom(const Request & rqst)
{
	RequestResult result;
	try {
		JoinRoomResponse response;
		JoinRoomRequest deserializedRequest = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(rqst.buffer);
		m_roomManager->joinRoom(m_user, deserializedRequest.roomId);
		result.newHandle = m_handlerFactory->createRoomMemberRequestHandler(m_user.getUsername(), deserializedRequest.roomId);
		response.status = true;
		result.response = JsonResponsePacketSerializer::serializeResponse(response);
	}
	catch (std::exception e) {
		error(e.what(), result);
	}
	return result;
}

RequestResult MenuRequestHandler::createRoom(const Request & rqst)
{
	RequestResult result;
	try {
		CreateRoomResponse response;
		CreateRoomRequest deserializedRequest = JsonRequestPacketDeserializer::deserializeCreateRoomReuquest(rqst.buffer);
		RoomData data;
		int roomID;
		data.isActive = false;
		data.name = deserializedRequest.roomName;
		data.maxPlayers = deserializedRequest.maxUsers;
		data.timePerQuestion = deserializedRequest.answerTimeout;
		data.questionsCount = deserializedRequest.questionCount;
		data.id = NULL;
		roomID = m_roomManager->createRoom(m_user, data);
		result.newHandle = m_handlerFactory->createRoomAdminRequestHandler(m_user.getUsername(), roomID);
		response.status = true;
		result.response = JsonResponsePacketSerializer::serializeResponse(response);
	}
	catch (std::exception e) {
		error(e.what(), result);
	}
	return result;
}

void MenuRequestHandler::error(const char * errorMsg, RequestResult & toReturn)
{
	toReturn.newHandle = m_handlerFactory->createMenuRequestHandler(m_user.getUsername());
	ErrorResponse errRes = { errorMsg };
	toReturn.response = JsonResponsePacketSerializer::serializeResponse(errRes);
}

bool MenuRequestHandler::isRequestRelevant(const Request& rqst) const
{
	if (SIGNOUT >= rqst.id && GET_HIGH_SCORES <= rqst.id ) {
		return true;
	}
	else{
		return false;
	}
}

RequestResult MenuRequestHandler::handleRequest(const Request& rqst)
{
	switch (rqst.id)
	{
	case SIGNOUT:
		return signout(rqst);
	case GET_ROOMS:
		return getRooms();
	case GET_PLAYERS_IN_ROOM:
		return getPlayersInRoom(rqst);
	case GET_HIGH_SCORES:
		return getHighscores();
	case JOIN_ROOM:
		return joinRoom(rqst);
	case CREATE_ROOM:
		return createRoom(rqst);
	default:
		//if the request isn't of any of the possible types
		RequestResult toReturn;
		toReturn.newHandle = nullptr;
		ErrorResponse errRes;
		errRes.message = "Error - request code not recognized";
		toReturn.response = JsonResponsePacketSerializer::serializeResponse(errRes);
		return toReturn;
	}
}

int MenuRequestHandler::getType()
{
	return MENU_REQUEST_HANDLER;
}

MenuRequestHandler::MenuRequestHandler(LoggedUser user, RoomManager * roomManager, HighscoreTable * highscoreTable, RequestHandlerFactory * handlerFactory):m_user(user), m_roomManager(roomManager), m_highscoreTable(highscoreTable), m_handlerFactory(handlerFactory)
{
}



