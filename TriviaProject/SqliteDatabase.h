#pragma once
#include <io.h>
#include <exception>
#include <random>
#include <set>
#include <mutex>
#include "IDatabase.h"
#include "sqlite3.h"

#define NOT_EXIST -1
#define EQUAL 0
#define NAME 1
#define SCORE 0
#define QUESTION_POS 0
#define CORRECT_ANSWER 0
#define OTHER_QUESTION2 1
#define OTHER_QUESTION3 2
#define OTHER_QUESTION4 3
#define START_AT_ZERO 1

enum tables {
	TURN_ON_FOREIGN_KEY,
	USER,
	QUESTION,
	GAME,
	HIGH_SCORES,
	NUM_TABLES
};


class SqliteDatabase: public IDatabase
{
	std::mutex openDB;
	bool open();
	sqlite3* m_db;
	std::random_device rd;
	std::mt19937 eng;
	std::uniform_int_distribution<> distr;
	std::vector<std::vector<std::string>> callbackUser(const char * command, std::vector<std::vector<std::string>>);
public:
	SqliteDatabase();
	~SqliteDatabase();
	virtual std::map<LoggedUser, int> getHighScores() override;
	virtual bool doesUserExist(std::string) override;
	bool isPasswordCorrect(std::string username, std::string password) override;
	virtual std::list<Question> getQuestion(int) override;
	virtual void addUser(std::string, std::string, std::string) override;
	virtual void addQuestion(std::string, std::vector<std::string>, int) override;
	virtual void addGame(int, std::string, std::string) override;
	virtual void addHighScore(int, std::string) override;
};

int callback(void * data, int argc, char ** argv, char ** azColName);

