#pragma once
#include "Response.h"
#include <mutex>

class TestResponse {
public:
	static void printMsg(std::vector<Byte> msgVec);

	static void checkResponse();
};

