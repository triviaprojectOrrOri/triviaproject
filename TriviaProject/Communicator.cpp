#include "Communicator.h"

char * Communicator::getClientMsg(SOCKET clientSoc, int& lenBuf)
{
	char *buffer = new char[MAX_LEN];
	lenBuf = recv(clientSoc, buffer, MAX_LEN, 0);
	if (INVALID_SOCKET == lenBuf) {
		int err = WSAGetLastError();
		if (CONNECTION_TERMINATED == err || 10038 == err)
			return nullptr;

		closesocket(clientSoc);

		return nullptr;
	}
	if (lenBuf == 0) {
		return nullptr;
	}
	return buffer;
}

void Communicator::sendMsg(SOCKET clientSoc, RequestResult res)
{

	int lenBuf = res.response.size();
	char* toSend = new char[lenBuf];
	for (int i = 0; i < lenBuf; i++) {
		toSend[i] = (char)res.response[i].to_ulong();
	}
	if (send(clientSoc, toSend, lenBuf, 0) == INVALID_SOCKET) {//sending the response to the client, checking if it fails
		closesocket(clientSoc);
		int err = WSAGetLastError();
		delete toSend;

		throw std::exception((std::string("send failed with error ") + std::to_string(err)).c_str());
	}
	delete toSend;
}

void Communicator::exitEverything(SOCKET clientSoc)
{
	if (ROOM_ADMIN_REQUEST_HANDLER == m_clients[clientSoc]->getType() || ROOM_MEMBER_REQUEST_HANDLER == m_clients[clientSoc]->getType()) {
		Request closeExitRoom;
		RequestResult res;
		closeExitRoom.id = CLOSE_OR_LEAVE_ROOM;
		closeExitRoom.buffer;
		closeExitRoom.receivalTime = std::time(0);
		res = m_clients[clientSoc]->handleRequest(closeExitRoom);
		delete m_clients[clientSoc];
		m_clients[clientSoc] = res.newHandle;
	}

	if (MENU_REQUEST_HANDLER == m_clients[clientSoc]->getType()) {
		Request signOutRqst;
		RequestResult res;
		signOutRqst.id = SIGNOUT;
		signOutRqst.buffer;
		signOutRqst.receivalTime = std::time(0);
		res = m_clients[clientSoc]->handleRequest(signOutRqst);
		delete m_clients[clientSoc];
		m_clients[clientSoc] = res.newHandle;
	}
}

void Communicator::handleClient(SOCKET clientSoc)
{
	int lenBuf;
	Request rqst;
	RequestResult res;
	while (true) {
		char * buffer = getClientMsg(clientSoc, lenBuf);
		if (buffer == nullptr) {
			exitEverything(clientSoc);
			return;
		}
		rqst.buffer = std::vector<Byte>(lenBuf);//setting the request data type with the correct values
		for (int i = 0; i < lenBuf; i++) {
			rqst.buffer[i] = buffer[i];
		}
		rqst.id = buffer[0];
		rqst.receivalTime = std::time(0);
		std::unique_lock<std::mutex> clientLock(clientsMtx);//to prevent multiple threads trying to access m_clients at once
		res = m_clients[clientSoc]->handleRequest(rqst);//handling the client's request
		delete m_clients[clientSoc];
		m_clients[clientSoc] = res.newHandle;
		clientLock.unlock();

		sendMsg(clientSoc, res);
		delete buffer;
	}
}

void Communicator::startThreadForNewClient(SOCKET clientSoc)
{
	std::thread *t = new std::thread(&Communicator::handleClient, this, clientSoc);
	if (m_numConnections != 0)
		threads.push_back(t);
	else {
		t->detach();
		delete t;
	}
	
}

void Communicator::setUpListenSoc(WSAData wsaData)
{
	int iResult;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	m_listenSoc = INVALID_SOCKET;


	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;//can communicate using IPv4
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;//communicating using TCP
	hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		
		throw std::exception((std::string("getaddrinfo failed with error ") + std::to_string(iResult)).c_str());
	}

	//creates a socket for listening
	m_listenSoc = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (INVALID_SOCKET == m_listenSoc) {
		int err = WSAGetLastError();
		
		throw std::exception((std::string("socket failed with error ") + std::to_string(err)).c_str());
	}

	// Setup the TCP listening socket
	iResult = bind(m_listenSoc, result->ai_addr, (int)result->ai_addrlen);
	if (SOCKET_ERROR == iResult) {
		freeaddrinfo(result);
		closesocket(m_listenSoc);
		int err = WSAGetLastError();
		
		throw std::exception((std::string("bind failed with error ") + std::to_string(err)).c_str());
	}

	freeaddrinfo(result);
}


Communicator::Communicator(IDatabase * db, int numConnections): m_handlerFactory(db), m_numConnections(numConnections)
{
}


Communicator::~Communicator()
{
	int i = 0;
	for (auto it = m_clients.begin(); it != m_clients.end(); it++, i++) {
		if (getNumToConnect() != NOT_TEST) {
			threads[i]->join();
			delete threads[i];
		}
		closesocket(it->first);
		delete it->second;
	}
	closesocket(m_listenSoc);
}

void Communicator::bindAndListen()
{
	WSADATA wsaData;
	int iResult;

	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (0 != iResult) {
		
		throw std::exception((std::string("WSAStartup failed with error ") + std::to_string(iResult)).c_str());
	}

	setUpListenSoc(wsaData);
	SOCKET clientSoc = INVALID_SOCKET;

	iResult = listen(m_listenSoc, SOMAXCONN);//making listenSoc activley listen
	if (SOCKET_ERROR == iResult) {
		closesocket(m_listenSoc);
		int err = WSAGetLastError();
		
		throw std::exception((std::string("listen failed with error ") + std::to_string(err)).c_str());
	}
}

void Communicator::handleRequests()
{
	SOCKET clientSoc = accept(m_listenSoc, NULL, NULL);
	if (INVALID_SOCKET == clientSoc) {
		closesocket(m_listenSoc);
		int err = WSAGetLastError();
		
		throw std::exception((std::string("accept failed with error ") + std::to_string(err)).c_str());
	}
	std::unique_lock<std::mutex> clientLock(clientsMtx);
	m_clients.insert(std::pair<SOCKET, IRequestHandler*>(clientSoc, m_handlerFactory.createLoginRequestHandler()));
	clientLock.unlock();
	startThreadForNewClient(clientSoc);
}
int Communicator::getNumToConnect() const
{
	return m_numConnections;
}

