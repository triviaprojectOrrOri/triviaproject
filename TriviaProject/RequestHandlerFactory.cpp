#include "RequestHandlerFactory.h"



RequestHandlerFactory::RequestHandlerFactory(IDatabase * db)
{
	m_loginManager = new LoginManager(db);
	m_roomManager = new RoomManager();
	m_highscoreTable = new HighscoreTable(db);
}


RequestHandlerFactory::~RequestHandlerFactory()
{
	delete m_loginManager;
	delete m_highscoreTable;
	delete m_roomManager;
}

LoginRequestHandler *RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(m_loginManager, this);
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(std::string username)
{
	return new MenuRequestHandler(LoggedUser(username), m_roomManager, m_highscoreTable, this);
}

RoomMemberRequestHandler *RequestHandlerFactory::createRoomMemberRequestHandler(std::string username, roomID id)
{
	return new RoomMemberRequestHandler(username, id, m_roomManager, this);
}

RoomAdminRequestHandler * RequestHandlerFactory::createRoomAdminRequestHandler(std::string username, roomID id)
{
	return new RoomAdminRequestHandler(username, id, m_roomManager, this);
}

