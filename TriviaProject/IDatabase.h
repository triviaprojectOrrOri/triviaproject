#pragma once
#include <map>
#include <list>
#include "LoggedUser.h"
#include "Question.h"
class IDatabase
{
public:
	IDatabase();
	~IDatabase();
	virtual std::map<LoggedUser, int> getHighScores() = 0;
	virtual bool doesUserExist(std::string) = 0;
	virtual bool isPasswordCorrect(std::string, std::string) = 0;
	virtual std::list<Question> getQuestion(int) = 0;
	virtual void addUser(std::string, std::string, std::string) = 0;
	virtual void addQuestion(std::string, std::vector<std::string>, int) = 0;
	virtual void addGame(int, std::string, std::string) = 0;
	virtual void addHighScore(int, std::string) = 0;
};
