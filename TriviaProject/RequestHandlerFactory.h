#pragma once
#include <string>
#include "LoginManager.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"

class LoginRequestHandler;
class MenuRequestHandler;
class RoomMemberRequestHandler;
class RoomAdminRequestHandler;

class RequestHandlerFactory
{
	LoginManager* m_loginManager;
	RoomManager* m_roomManager;
	HighscoreTable* m_highscoreTable;
public:
	RequestHandlerFactory(IDatabase * db);
	~RequestHandlerFactory();
	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(std::string username);//requires the username of the player
	RoomMemberRequestHandler *createRoomMemberRequestHandler(std::string username, roomID id);
	RoomAdminRequestHandler *createRoomAdminRequestHandler(std::string username, roomID id);
	LoginManager *getLoginManager() const { return m_loginManager; };
};

